package com.example.androiddemo.network;


import com.example.androiddemo.model.Account;
import com.example.androiddemo.model.LoginRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIManager {
    String SERVER_URL="http://bms.mis.vn/";
    @POST("api/MobileNhanVien/DangNhap")
    Call<LoginRequest>Login(@Body Account account);


}
