package com.example.androiddemo.model;

import java.io.Serializable;

public class Account implements Serializable {
    public String getTenDangNhap() {
        return TenDangNhap;
    }

    public void setTenDangNhap(String tenDangNhap) {
        TenDangNhap = tenDangNhap;
    }

    public String getMatKhau() {
        return MatKhau;
    }

    public void setMatKhau(String matKhau) {
        MatKhau = matKhau;
    }

    public String getMaMay() {
        return MaMay;
    }

    public void setMaMay(String maMay) {
        MaMay = maMay;
    }

    public String getMaHeDieuHanh() {
        return MaHeDieuHanh;
    }

    public void setMaHeDieuHanh(String maHeDieuHanh) {
        MaHeDieuHanh = maHeDieuHanh;
    }

    public String getTenTokenFirebase() {
        return TokenFirebase;
    }

    public void setTenTokenFirebase(String tenTokenFirebase) {
        TokenFirebase = tenTokenFirebase;
    }

    public Account(String tenDangNhap, String matKhau, String maMay, String maHeDieuHanh, String tenTokenFirebase) {
        TenDangNhap = tenDangNhap;
        MatKhau = matKhau;
        MaMay = maMay;
        MaHeDieuHanh = maHeDieuHanh;
        TokenFirebase = tenTokenFirebase;
    }

    private String TenDangNhap;
    private String MatKhau;
    private String MaMay;
    private String MaHeDieuHanh;
    private String TokenFirebase;
}
