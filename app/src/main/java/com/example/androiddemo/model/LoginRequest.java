package com.example.androiddemo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public  class LoginRequest {

    @Expose
    @SerializedName("DanhSachDonVi")
    public List<DanhSachDonVi> DanhSachDonVi;
    @Expose
    @SerializedName("UserInfo")
    public UserInfo UserInfo;

    public List<LoginRequest.DanhSachDonVi> getDanhSachDonVi() {
        return DanhSachDonVi;
    }

    public void setDanhSachDonVi(List<LoginRequest.DanhSachDonVi> danhSachDonVi) {
        DanhSachDonVi = danhSachDonVi;
    }

    public LoginRequest.UserInfo getUserInfo() {
        return UserInfo;
    }

    public void setUserInfo(LoginRequest.UserInfo userInfo) {
        UserInfo = userInfo;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getErrorDesc() {
        return ErrorDesc;
    }

    public void setErrorDesc(String errorDesc) {
        ErrorDesc = errorDesc;
    }

    public int getErrorCode() {
        return ErrorCode;
    }

    public void setErrorCode(int errorCode) {
        ErrorCode = errorCode;
    }

    @Expose
    @SerializedName("Token")
    public String Token;
    @Expose
    @SerializedName("ErrorDesc")
    public String ErrorDesc;
    @Expose
    @SerializedName("ErrorCode")
    public int ErrorCode;

    public  class DanhSachDonVi {
        @Expose
        @SerializedName("DuongDanAnh")
        public String DuongDanAnh;
        @Expose
        @SerializedName("DiaChi")
        public String DiaChi;
        @Expose
        @SerializedName("Ten")
        public String Ten;
        @Expose
        @SerializedName("Id")
        public int Id;
    }

    public  class UserInfo {
        public String getSoDienThoai() {
            return SoDienThoai;
        }

        public void setSoDienThoai(String soDienThoai) {
            SoDienThoai = soDienThoai;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getHoVaTen() {
            return HoVaTen;
        }

        public void setHoVaTen(String hoVaTen) {
            HoVaTen = hoVaTen;
        }

        public String getTenDangNhap() {
            return TenDangNhap;
        }

        public void setTenDangNhap(String tenDangNhap) {
            TenDangNhap = tenDangNhap;
        }

        public int getId() {
            return Id;
        }

        public void setId(int id) {
            Id = id;
        }

        @Expose
        @SerializedName("SoDienThoai")
        public String SoDienThoai;
        @Expose
        @SerializedName("Email")
        public String Email;
        @Expose
        @SerializedName("HoVaTen")
        public String HoVaTen;
        @Expose
        @SerializedName("TenDangNhap")
        public String TenDangNhap;
        @Expose
        @SerializedName("Id")
        public int Id;
    }
}
