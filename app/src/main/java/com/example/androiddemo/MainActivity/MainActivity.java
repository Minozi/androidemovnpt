package com.example.androiddemo.MainActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.androiddemo.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}