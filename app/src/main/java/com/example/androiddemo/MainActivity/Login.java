package com.example.androiddemo.MainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.androiddemo.R;
import com.example.androiddemo.model.Account;
import com.example.androiddemo.model.LoginRequest;
import com.example.androiddemo.network.APIManager;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Order;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Login extends AppCompatActivity implements Validator.ValidationListener {


    @Order(1)
    @NotEmpty(message = "Chưa nhập tài khoản đăng nhập")
    @BindView(R.id.edtUser)
    EditText edtUser;


    @Order(2)
    @NotEmpty(message = "Bạn chưa nhập mật khẩu")
    @Password(min = 8, message = "Mật khẩu tối thiểu 8 kí tự")
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.ivCover)
    ImageView ivCover;
    @BindView(R.id.tvDangNhap)
    TextView tvDangNhap;
    @BindView(R.id.tvTDN)
    TextView tvTDN;
    @BindView(R.id.tvMK)
    TextView tvMK;
    @BindView(R.id.ivCover1)
    ImageView ivCover1;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.tvQuyenMK)
    TextView tvQuyenMK;
    @BindView(R.id.ivCover2)
    ImageView ivCover2;
    @BindView(R.id.tvVanTay)
    TextView tvVanTay;
    @BindView(R.id.ivCover3)
    ImageView ivCover3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.btnLogin, R.id.tvQuyenMK})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                Login();

                break;
            case R.id.tvQuyenMK:
                break;
        }
    }

    private void Login() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIManager service = retrofit.create(APIManager.class);
        service.Login(new Account(edtUser.getText().toString(), etPassword.getText().toString(), "14843461661464", "IOS", "dfh;aguoafgmagp'gaafa64dgfgsgs6fg44s3g2sf9g")).enqueue(new Callback<LoginRequest>() {
            @Override
            public void onResponse(Call<LoginRequest> call, Response<LoginRequest> response) {
                if (response.body() != null) {
                    LoginRequest loginRequest=(LoginRequest)response.body();
                    return;
                }

                Intent intent = new Intent(Login.this, MainActivity.class);
                startActivity(intent);



            }

            @Override
            public void onFailure(Call<LoginRequest> call, Throwable t) {
                Toast.makeText(Login.this, "Fail", Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        Toast.makeText(this, "Yay! we got it right!", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }


}
